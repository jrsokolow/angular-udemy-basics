import { Component } from '@angular/core';

@Component({
  selector:'[username]',
  template:`
    <input type="text" [(ngModel)]="userName" />
    <p>{{userName}}</p>
    <button class="btn btn-primary" [disabled]="disableResetButton()" (click)="resetUserName()">Reset user name</button>
  `
})
export class UsernameComponent {

  userName = '';

  disableResetButton() {
    return this.userName.length === 0;
  }

  resetUserName() {
    this.userName = '';
  }

}
