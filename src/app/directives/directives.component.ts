import { Component } from '@angular/core';

@Component({
  selector:'directives',
  template:`
    <button class="btn btn-primary" (click)="showDetails()">Display details</button>
    <p *ngIf="canDisplayDetails">Secret password = tuna</p>
    <p [ngStyle]="{backgroundColor:i > 4 ? 'blue' : 'white'}" *ngFor="let log of logs; let i = index;">{{log}}</p>
  `
})
export class DirectivesComponent {
  canDisplayDetails = false;
  logs = [];
  counter = 0;

  showDetails() {
    this.canDisplayDetails = !this.canDisplayDetails;
    this.counter += 1;
    this.logs.push('Button clicked n-times: ' + this.counter);
  }

}
